#[macro_use]
extern crate macro_rules_attribute;

use anyhow::Result;
use std::convert::TryInto;
use structs::{
    js_value_from_ws,
    structs_db::{DbBlock, DbTransaction},
    structs_js::{JsErr, JsErrWrapper},
    structs_ws::{SubscribeTypes, WsSender},
};
use wasm_bindgen::prelude::*;

mod structs;
#[cfg(test)]
mod test;
mod utils;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

fn decode_ws(v: &[u8]) -> Result<JsValue> {
    // convert the bytes into WsSender struct
    let ws_sender: WsSender = v.try_into()?;

    // match on SubscribeTypes to deserialize into proper struct
    let js_value = match ws_sender.data_type {
        SubscribeTypes::Block => js_value_from_ws::<DbBlock>(ws_sender)?,
        SubscribeTypes::Transaction => js_value_from_ws::<DbTransaction>(ws_sender)?,
    };
    Ok(js_value)
}

#[wasm_bindgen]
pub fn deserialize_ws(v: &[u8]) -> JsValue {
    match decode_ws(v) {
        Ok(js_value) => js_value,
        Err(e) => JsValue::from_serde(&JsErrWrapper {
            err: JsErr { message: e.to_string() },
        })
        .unwrap(),
    }
}
