use self::{
    structs_js::JsDataWrapper,
    structs_ws::{WsSender, WsSenderDecoded},
};

use anyhow::Result;
use std::convert::{TryFrom, TryInto};
use wasm_bindgen::JsValue;

pub mod structs_db;
pub mod structs_js;
pub mod structs_ws;

// This will convert the data byte array within WsSender into its type and serialize it into a JsValue
pub fn js_value_from_ws<T>(ws_sender: WsSender) -> Result<JsValue>
where
    for<'a> T: TryFrom<&'a [u8], Error = bincode::Error>,
    T: serde::Serialize,
{
    // let w = <WsSenderDecoded<T>>::try_from(ws_sender)?;
    let w: WsSenderDecoded<T> = ws_sender.try_into()?;
    let x = JsValue::from_serde(&JsDataWrapper::new(w))?;
    Ok(x)
}

// This macro is used on the DbBlock, DbTransaction structs.
// It allows a [u8] byte array to deserialize into the struct
// specifically this is used to satisfy the TryFrom constraint in `js_value_from_ws`
#[macro_export]
macro_rules! serialize_from_bytes {
    (
        $(#[$struct_meta:meta])*
        $struct_vis:vis
        struct $StructName:ident {
            $(
                $(#[$field_meta:meta])*
                $field_vis:vis $field_name:ident : $field_ty:ty
            ),* $(,)?
        }
    ) => (
        // generate the struct definition we have been given
        $(#[$struct_meta])*
        $struct_vis
        struct $StructName {
            $(
                $(#[$field_meta])*
                $field_vis $field_name: $field_ty,
            )*
        }
        // add the impl from &[u8] bytes through bincode
        impl TryFrom<&[u8]> for $StructName {
            type Error = bincode::Error;
            fn try_from(v: &[u8]) -> Result<Self, Self::Error> {
                Ok(bincode::deserialize(&v)?)
            }
        }
    )
}
