use hex::ToHex;
use serde::{de, Deserialize, Deserializer, Serialize, Serializer};
use std::{
    array::TryFromSliceError,
    convert::{TryFrom, TryInto},
};

use crate::serialize_from_bytes;

pub type TypeFixedBytes32 = [u8; 32];
pub type TypeFixedBytes21 = [u8; 21];
pub type TypeFixedBytes65 = [u8; 65];

#[macro_rules_attribute(serialize_from_bytes!)]
#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct DbBlock {
    pub id: i64,
    pub shard_id: i16,
    pub block_level: i64,
    #[serde(serialize_with = "buffer_to_hex")]
    pub block_hash: TypeFixedBytes32,
    #[serde(serialize_with = "buffer_to_hex")]
    pub block_hash_previous: TypeFixedBytes32,
    pub block_timestamp: i64,
    pub producer: i32,
    pub committee_id: i16,
    pub transaction_count: i32,
}
#[macro_rules_attribute(serialize_from_bytes!)]
#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct DbTransaction {
    pub id: i64,
    #[serde(serialize_with = "buffer_to_hex")]
    pub transaction_hash: TypeFixedBytes32,
    pub shard_id: i16,
    pub block_level: i64,
    #[serde(serialize_with = "buffer_to_hex")]
    pub block_hash: TypeFixedBytes32,
    pub block_timestamp: i64,
    #[serde(serialize_with = "buffer_to_hex", deserialize_with = "bytes_to_fixed_bytes")]
    pub trx_signature: TypeFixedBytes65,
    #[serde(serialize_with = "buffer_to_hex")]
    pub address_from: TypeFixedBytes21,
    #[serde(serialize_with = "buffer_to_hex")]
    pub header_contract: TypeFixedBytes21,
    pub header_payload_length: i32,
    pub inner_nonce: i32,
    pub inner_valid_to: i64,
    pub inner_cost: i32,
    // I am creating an option for payload_data as this can be large and most times don't need it
    #[serde(serialize_with = "opt_buffer_to_hex")]
    pub payload_data: Option<Vec<u8>>,
    pub is_public_contract: bool,
}

// Create the custom serializers
pub fn buffer_to_hex<S>(buffer: &[u8], serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let hex_str = buffer.encode_hex::<String>();
    serializer.serialize_str(&hex_str)
}
pub fn opt_buffer_to_hex<S, T>(opt: &Option<T>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
    T: AsRef<[u8]>,
{
    match opt {
        Some(buffer) => {
            let hex_str = hex::encode(buffer);
            serializer.serialize_str(&hex_str)
        }
        None => serializer.serialize_none(),
    }
}

pub fn bytes_to_fixed_bytes<'a, D, T>(d: D) -> Result<T, D::Error>
where
    D: Deserializer<'a>,
    for<'b> T: std::convert::TryFrom<&'b [u8], Error = TryFromSliceError>,
{
    let bytes = <&[u8]>::deserialize(d)?;
    match bytes.try_into() {
        Ok(b) => Ok(b),
        Err(e) => Err(de::Error::custom(e)),
    }
}
