use serde::Serialize;

#[derive(Serialize)]
pub struct JsErrWrapper {
    pub err: JsErr,
}
#[derive(Serialize)]
pub struct JsErr {
    pub message: String,
}
#[derive(Serialize)]
pub struct JsDataWrapper<T> {
    data: T,
}
impl<T> JsDataWrapper<T> {
    pub fn new(t: T) -> Self {
        Self { data: t }
    }
}
