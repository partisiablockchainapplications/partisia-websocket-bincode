use super::structs_db::TypeFixedBytes21;

use anyhow::Result;
use hex::ToHex;
use serde::{ser::SerializeSeq, Deserialize, Serialize, Serializer};
use std::{convert::TryFrom, mem};

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
#[repr(u8)]
pub enum SubscribeTypes {
    Block,
    Transaction,
}
#[derive(Deserialize, Serialize)]
pub struct WsSender {
    pub data_type: SubscribeTypes,
    pub data: Vec<u8>,
    pub sender: Option<TypeFixedBytes21>,
    pub recipients: Option<Vec<TypeFixedBytes21>>,
}
impl TryFrom<&[u8]> for WsSender {
    type Error = bincode::Error;
    fn try_from(v: &[u8]) -> Result<Self, Self::Error> {
        Ok(bincode::deserialize(&v)?)
    }
}
#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct WsSenderDecoded<T> {
    pub data_type: SubscribeTypes,
    #[serde(rename(serialize = "payload"))]
    pub data: T,
    #[serde(serialize_with = "sender_to_hex", skip_serializing_if = "Option::is_none")]
    pub sender: Option<TypeFixedBytes21>,
    #[serde(serialize_with = "vec_sender_to_hex", skip_serializing_if = "Option::is_none")]
    pub recipients: Option<Vec<TypeFixedBytes21>>,
}

impl<T> TryFrom<WsSender> for WsSenderDecoded<T>
where
    for<'a> T: std::convert::TryFrom<&'a [u8], Error = bincode::Error>,
{
    type Error = bincode::Error;

    #[inline]
    fn try_from(mut ws_sender: WsSender) -> Result<Self, Self::Error> {
        // take ownership of data from ws_sender to later decode
        let v = mem::replace(&mut ws_sender.data, Vec::new());
        let data = T::try_from(v.as_slice())?;

        // Build WsSenderDecoded struct with data
        let decoded = WsSenderDecoded {
            data,
            data_type: ws_sender.data_type,
            sender: ws_sender.sender,
            recipients: ws_sender.recipients,
        };
        Ok(decoded)
    }
}

pub fn sender_to_hex<S>(buffer: &Option<TypeFixedBytes21>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match buffer {
        Some(buf) => {
            let hex_str = buf.encode_hex::<String>();
            serializer.serialize_str(&hex_str)
        }
        // should be unreachable because skip_serializing_if = "Option::is_none"
        None => serializer.serialize_none(),
    }
}
// https://serde.rs/impl-serialize.html
pub fn vec_sender_to_hex<S>(vec_buffers: &Option<Vec<TypeFixedBytes21>>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match vec_buffers {
        Some(buf) => {
            let mut seq = serializer.serialize_seq(Some(buf.len()))?;
            for e in buf {
                let hex_str = e.encode_hex::<String>();
                seq.serialize_element(&hex_str)?;
            }
            seq.end()
        }
        // should be unreachable because skip_serializing_if = "Option::is_none"
        None => serializer.serialize_none(),
    }
}
