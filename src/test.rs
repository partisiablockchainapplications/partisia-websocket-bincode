use super::*;
use crate::structs::structs_ws::WsSenderDecoded;

use wasm_bindgen_test::*;

#[wasm_bindgen_test]
fn decode_block() {
    // Block of bytes to deserialize
    let bytes = hex_literal::hex!("000000006400000000000000e8030000000000000100800d0000000000004d2660184b06f1ceb04294f6c3f1fc0d389a4eef4eac8c7d347a9d69750ed8500d389a4eef4eac8c7d347a9d69750ed84d2660184b06f1ceb04294f6c3f1fc50970c8ad97d010000110000000200320000000000");

    // Convert bytes to WsSender
    let ws_sender: WsSender = bytes.as_slice().try_into().unwrap();

    // WsSender data_type should be Block
    assert_eq!(ws_sender.data_type, SubscribeTypes::Block);

    // convert WsSender into WsSenderDecoded
    let decoded: WsSenderDecoded<DbBlock> = ws_sender.try_into().unwrap();

    assert_eq!(decoded.data.id, 1000);
    assert_eq!(decoded.data.shard_id, 1);
    assert_eq!(decoded.data.block_level, 3456);
    assert_eq!(decoded.data.block_hash, hex_literal::hex!("4d2660184b06f1ceb04294f6c3f1fc0d389a4eef4eac8c7d347a9d69750ed850"));
    assert_eq!(decoded.data.block_hash_previous, hex_literal::hex!("0d389a4eef4eac8c7d347a9d69750ed84d2660184b06f1ceb04294f6c3f1fc50"));
    assert_eq!(decoded.data.block_timestamp, 1640032242839);
    assert_eq!(decoded.data.producer, 17);
    assert_eq!(decoded.data.committee_id, 2);
    assert_eq!(decoded.data.transaction_count, 50);

    // independently deserialize bytes from the wasm function then back into the struct
    let _js_value = deserialize_ws(&bytes);
    // let from_js_value = js_value.into_serde::<WsSenderDecoded<DbBlock>>().unwrap();

    // assert_eq!(&from_js_value, &decoded);

    // println!("{}", serde_json::to_string_pretty(&decoded).unwrap());
}

#[wasm_bindgen_test]
fn decode_transaction() {
    let bytes = hex_literal::hex!("01000000290100000000000057810000000000006233b77cdef13ceb6354e0260bdbe4663a863890dfa10d9209936821f5ff4aad0200862b0000000000004d2660184b06f1ceb04294f6c3f1fc0d389a4eef4eac8c7d347a9d69750ed850970c8ad97d010000410000000000000001215ce75ae2f097be5c264c124c1c28e5b88d05760279da82e706bf6c87bef62f5fe5c0a7c043e1f88ba15af34ad5d5e07eb5fd55fe77010974219cc432791f9f007b2a1674af4548d5e4744278d19df6f4c63881e1045dbd4c13df987d7fb4450e54bcd94b34a80f23513e0000001f0200003bf68ad97d01000000000000013e00000000000000000000000000000008006ab1d227a91f63fb0a2e257774ed2a179f332f220000000000000000000000000000000000000000000000000186cc6acd4b00000001045dbd4c13df987d7fb4450e54bcd94b34a80f2351010100000000000000045dbd4c13df987d7fb4450e54bcd94b34a80f2351");

    // Convert bytes to WsSender
    let ws_sender: WsSender = bytes.as_slice().try_into().unwrap();

    // WsSender data_type should be Block
    assert_eq!(ws_sender.data_type, SubscribeTypes::Transaction);

    // convert WsSender into WsSenderDecoded
    let decoded: WsSenderDecoded<DbTransaction> = ws_sender.try_into().unwrap();

    assert_eq!(decoded.data.id, 33111);
    assert_eq!(decoded.data.transaction_hash, hex_literal::hex!("6233b77cdef13ceb6354e0260bdbe4663a863890dfa10d9209936821f5ff4aad"));
    assert_eq!(decoded.data.shard_id, 2);
    assert_eq!(decoded.data.block_level, 11142);
    assert_eq!(decoded.data.block_hash, hex_literal::hex!("4d2660184b06f1ceb04294f6c3f1fc0d389a4eef4eac8c7d347a9d69750ed850"));
    assert_eq!(decoded.data.block_timestamp, 1640032242839);
    assert_eq!(
        decoded.data.trx_signature,
        hex_literal::hex!("01215ce75ae2f097be5c264c124c1c28e5b88d05760279da82e706bf6c87bef62f5fe5c0a7c043e1f88ba15af34ad5d5e07eb5fd55fe77010974219cc432791f9f")
    );
    assert_eq!(decoded.data.address_from, hex_literal::hex!("007b2a1674af4548d5e4744278d19df6f4c63881e1"));
    assert_eq!(decoded.data.header_contract, hex_literal::hex!("045dbd4c13df987d7fb4450e54bcd94b34a80f2351"));
    assert_eq!(decoded.data.header_payload_length, 62);
    assert_eq!(decoded.data.inner_nonce, 543);
    assert_eq!(decoded.data.inner_valid_to, 1640032302651);
    assert_eq!(decoded.data.inner_cost, 0);
    assert_eq!(
        decoded.data.payload_data,
        Some(hex_literal::hex!("000000000000000008006ab1d227a91f63fb0a2e257774ed2a179f332f220000000000000000000000000000000000000000000000000186cc6acd4b0000").to_vec())
    );
    assert_eq!(decoded.data.is_public_contract, false);

    // independently deserialize bytes from the wasm function then back into the struct
    let _js_value = deserialize_ws(&bytes);
    // let from_js_value = js_value.into_serde::<WsSenderDecoded<DbTransfer>>().unwrap();

    // assert_eq!(&from_js_value, &decoded);
}
